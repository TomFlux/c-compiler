// https://norasandler.com/2017/11/29/Write-a-Compiler.html

/*
==== AST Nodes ====
<program>   ::= <function>
<function>  ::= "int" <id> "(" ")" "{" <statement> "}"
<statement> ::= "return" <exp> ";"
<exp>       ::= <int>
*/

trait Node {
    fn to_assembly(&self) -> String;
}

struct Program {
    Main: Function,
}
struct Function {
    Id: Id,
    Statements: Vec<Statement>,
}
struct Statement {
    Expression: Exp,
}
struct Id(String);
struct Exp(i32);

impl Node for Program {
    fn to_assembly(&self) -> String {
        format!(" .global {}\n{}:", self.Main.Id.0, self.Main.Id.0)
    }
}

const KEYWORDS: [&str; 2] = ["return", "int"];

fn debug_pprint_ast(prog: &Program) {
    let mut out_str: String = String::from("PROG:");

    out_str.push_str("\n\tFUNC INT ");
    out_str.push_str(&prog.Main.Id.0);
    out_str.push_str("\n\t\tbody:");

    for s in prog.Main.Statements.iter() {
        out_str.push_str("\n\t\t\tSTATE RETURN ");
        out_str.push_str(&s.Expression.0.to_string());
    }

    println!("{}", out_str);
}

fn check_end(c: &str) -> bool {
    let ends: Vec<&str> = vec!["{", "}", "(", ")", "\t", " ", ";"];

    if ends.iter().any(|&e| e == c) {
        true
    } else {
        false
    }
}

fn check_unprintable(to_push: &String) -> bool {
    let unprintables: Vec<&str> = vec!["", " ", "\n", "\t"];

    if unprintables.iter().any(|&c| c == to_push) {
        true
    } else {
        false
    }
}

fn check_keyword(potential: &String) -> bool {
    if KEYWORDS.iter().any(|kw| &kw.to_string() == potential) {
        true
    } else {
        false
    }
}

fn lex(s: &str) -> Vec<String> {
    let mut matches: Vec<String> = Vec::new();

    let split_source = s.split("");
    let mut buffer: String = String::new();

    for new_chr in split_source {
        if check_end(new_chr) {
            if !check_unprintable(&buffer) {
                if check_keyword(&buffer) {
                    matches.push(buffer);
                } else {
                    matches.push(buffer);
                }
            }

            if !check_unprintable(&new_chr.to_string()) {
                matches.push(new_chr.to_string());
            }

            buffer = String::new();
        } else {
            buffer.push_str(new_chr);
        }
    }

    matches
}

// <exp>       ::= <int>
fn parse_exp(token: &String) -> Exp {
    let exp: Exp = match token.parse::<i32>() {
        Err(_) => panic!("Tried to parse '{}' as int when creating expression", token),
        Ok(n) => Exp(n),
    };

    exp
}

// <statement> ::= "return" <exp> ";"
fn parse_statement(p_ret: &String, p_exp: &String, p_semi: &String) -> Statement {
    if p_ret != "return" {
        panic!("First arguement was not the keyword return.")
    }

    if p_semi != ";" {
        panic!("Third arguement was not a semicolon.")
    }

    let exp: Exp = parse_exp(p_exp);

    let state: Statement = Statement { Expression: exp };

    state
}

// <function>  ::= "int" <id> "(" ")" "{" <statement> "}"
fn parse_func(tokens: &Vec<String>) -> Function {
    if tokens[0] != "int" {
        panic!("Function can only return int.")
    }
    if tokens[2] != "(" {
        panic!("ID should be followed by (")
    }
    if tokens[3] != ")" {
        panic!("( should be followed by )")
    }
    if tokens[4] != "{" {
        panic!(") should be followed by an open brace")
    }
    if tokens[tokens.len() - 1] != "}" {
        panic!("Final token should be a close brace")
    }

    let id: Id = Id(tokens[1].clone());

    let body = &tokens[5..tokens.len() - 1];
    let mut buff: Vec<&String> = Vec::new();
    let mut statements: Vec<Statement> = Vec::new();

    for tok in body.iter() {
        if buff.len() != 2 {
            buff.push(tok);
        } else {
            buff.push(tok);
            let new_state: Statement = parse_statement(buff[0], buff[1], buff[2]);
            statements.push(new_state);

            buff = Vec::new();
        }
    }

    let func: Function = Function {
        Id: id,
        Statements: statements,
    };

    func
}

// <program>   ::= <function>
fn parse_prog(tokens: &Vec<String>) -> Program {
    let func: Function = parse_func(tokens);

    let prog: Program = Program { Main: func };

    prog
}

fn code_generation(prog: &Program) -> String {
    let mut code: String = String::from(" .global ");

    code.push_str(&prog.Main.Id.0);
    code.push_str("\n");
    code.push_str(&prog.Main.Id.0);
    code.push_str(":\n");

    for s in prog.Main.Statements.iter() {
        code.push_str(" mov $");
        code.push_str(&s.Expression.0.to_string());
        code.push_str(", %eax");
        code.push_str("\n");
    }

    code.push_str(" ret\n");

    code
}

fn main() {
    /* pprint of source
    int main() {
        return 2;
    }
    */
    let source = "int main() {\n\treturn 2;\n}";

    let tokens: Vec<String> = lex(source);

    println!("Found these tokens");
    for tok in tokens.iter() {
        println!("{}", tok);
    }

    let prog: Program = parse_prog(&tokens);

    println!("{}", code_generation(&prog));
}
